package com.riyazm.swiggyassignment.utils;

import android.text.TextUtils;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.riyazm.swiggyassignment.ApplicationController;

/**
 * Created by muhammadriyaz on 12/02/17.
 */

public class APIs {

    public static final String TAG = APIs.class.getSimpleName();

    /**
     * @param url           where to hit
     * @param method        by what method to hit Method.GET or Method.POST etc..,
     * @param tagJsonObject tag in case if you would like to cancel this request from queue
     * @param callbacks     register them to handle the api outcome
     */

    public static void requestString(final String url, int method, String tagJsonObject, final APICallbacks callbacks) {
        StringRequest stringReq = new StringRequest(method,
                url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // strip off unwanted chars
                        String temp = response;
                        if (!TextUtils.isEmpty(response) && response.startsWith("jsonFlickrFeed(")) {
                            temp = temp.replace("jsonFlickrFeed(", "");
                            temp = temp.substring(0, temp.length() - 1);
                        }

                        callbacks.onPostExecuteSuccess(url, temp);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callbacks.onPostExecuteFail(url, error);
            }
        });

        // Adding request to request queue
        callbacks.onPreExecute(url);
        ApplicationController.getInstance().addToRequestQueue(stringReq, tagJsonObject);
    }
}