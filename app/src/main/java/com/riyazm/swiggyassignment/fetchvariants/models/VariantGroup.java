
package com.riyazm.swiggyassignment.fetchvariants.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class VariantGroup {

    @SerializedName("group_id")
    private String mGroupId;
    @SerializedName("name")
    private String mName;
    @SerializedName("variations")
    private List<Variation> mVariations;

    public String getGroupId() {
        return mGroupId;
    }

    public String getName() {
        return mName;
    }

    public List<Variation> getVariations() {
        return mVariations;
    }

    public static class Builder {

        private String mGroupId;
        private String mName;
        private List<Variation> mVariations;

        public VariantGroup.Builder withGroupId(String groupId) {
            mGroupId = groupId;
            return this;
        }

        public VariantGroup.Builder withName(String name) {
            mName = name;
            return this;
        }

        public VariantGroup.Builder withVariations(List<Variation> variations) {
            mVariations = variations;
            return this;
        }

        public VariantGroup build() {
            VariantGroup VariantGroup = new VariantGroup();
            VariantGroup.mGroupId = mGroupId;
            VariantGroup.mName = mName;
            VariantGroup.mVariations = mVariations;
            return VariantGroup;
        }

    }

    @Override
    public String toString() {
        return "VariantGroup{" +
                "mGroupId='" + mGroupId + '\'' +
                ", mName='" + mName + '\'' +
                ", mVariations=" + mVariations +
                '}';
    }
}
