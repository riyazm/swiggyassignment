package com.riyazm.swiggyassignment.fetchvariants;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.riyazm.swiggyassignment.R;
import com.riyazm.swiggyassignment.fetchvariants.models.Variants;
import com.riyazm.swiggyassignment.utils.APICallbacks;
import com.riyazm.swiggyassignment.utils.APIs;
import com.riyazm.swiggyassignment.utils.EndPoints;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by muhammadriyaz on 09/05/17.
 */

public class FetchPresenter {
    private FetchView view;


    public FetchPresenter(FetchView view) {
        this.view = view;
    }

    public void getData() {
        String tag_json_obj = "variant_list_request";
        APIs.requestString(EndPoints.GET_VARIANTS, Request.Method.GET, tag_json_obj, new APICallbacks() {
            @Override
            public void onPreExecute(String requestURL) {
                super.onPreExecute(requestURL);
                view.setLoaderVisibility(true);
            }

            @Override
            public void onPostExecuteSuccess(String requestURL, String response) {
                super.onPostExecuteSuccess(requestURL, response);
                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject variantsJSONObject = object.optJSONObject("variants");
                    Gson gson = new Gson();
                    Variants variants = gson.fromJson(variantsJSONObject.toString(), Variants.class);
                    if (variants != null) {
                        JSONArray excludeJSONArray = variantsJSONObject.optJSONArray("exclude_list");
                        Map<String, String> excludeMaps = new HashMap<>();
                        for (int i = 0; i < excludeJSONArray.length(); i++) {
                            JSONArray excludedCombinationArray = excludeJSONArray.optJSONArray(i);
                            if (excludedCombinationArray.length() == 2) {
                                JSONObject objectKey = excludedCombinationArray.optJSONObject(0);
                                JSONObject objectValue = excludedCombinationArray.optJSONObject(1);
                                excludeMaps.put(objectKey.optString("variation_id"),
                                        objectValue.optString("variation_id"));
                            }
                        }
                        view.printVariantGroups(variants.getVariantGroups(), excludeMaps);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                view.setLoaderVisibility(false);
            }

            @Override
            public void onPostExecuteFail(String requestURL, VolleyError error) {
                super.onPostExecuteFail(requestURL, error);
                view.showError(R.string.something_went_wrong_please_try_again);
                // hide the progress dialog
                view.setLoaderVisibility(false);
            }
        });
    }
}
