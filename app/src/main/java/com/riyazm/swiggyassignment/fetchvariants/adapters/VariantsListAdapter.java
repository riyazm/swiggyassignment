package com.riyazm.swiggyassignment.fetchvariants.adapters;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.riyazm.swiggyassignment.R;
import com.riyazm.swiggyassignment.fetchvariants.ItemTapListener;
import com.riyazm.swiggyassignment.fetchvariants.models.VariantGroup;
import com.riyazm.swiggyassignment.fetchvariants.models.Variation;

import java.util.List;

/**
 * Created by muhammadriyaz on 09/05/17.
 */

public class VariantsListAdapter extends RecyclerView.Adapter<VariantsListAdapter.ViewHolder> {

    private Context context;
    private List<VariantGroup> items;
    ItemTapListener listener;

    public VariantsListAdapter(Context context, List<VariantGroup> items) {
        this.context = context;
        this.items = items;
    }

    public void setOnTapListener(ItemTapListener itemTapListener) {
        this.listener = itemTapListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_fetch_list_adapter_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(mView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.title.setText(items.get(position).getName());
        holder.radioButtons = new RadioButton[items.get(position).getVariations().size()];
        holder.variationList.removeAllViews();
        for (int i = 0; i < items.get(position).getVariations().size(); i++) {
            Variation variation = items.get(position).getVariations().get(i);
            holder.radioButtons[i] = new RadioButton(context);
            holder.radioButtons[i].setTag(variation.getId());
            holder.radioButtons[i].setChecked(variation.getDefault() == 1);
            LinearLayoutCompat.LayoutParams params = new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params.topMargin = 20;
            holder.radioButtons[i].setLayoutParams(params);
            holder.radioButtons[i].setText("Name: " + variation.getName() + "\n" +
                    "Price: " + variation.getPrice() + "\n" +
                    "In Stock:" + (variation.getInStock() == 1 ? "Yes" : "No")
            );
            holder.variationList.setTag(items.get(position).getGroupId());
            holder.variationList.addView(holder.radioButtons[i]);
        }
        holder.variationList.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if (listener != null && radioGroup.findViewById(i) != null) {
                    listener.onTap(radioGroup.findViewById(i).getTag().toString(), radioGroup.getTag().toString());
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        RadioGroup variationList;
        RadioButton[] radioButtons;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            variationList = (RadioGroup) itemView.findViewById(R.id.variation_list);
        }
    }
}
