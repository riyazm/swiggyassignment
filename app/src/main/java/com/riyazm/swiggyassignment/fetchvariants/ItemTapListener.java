package com.riyazm.swiggyassignment.fetchvariants;

/**
 * Created by muhammadriyaz on 09/05/17.
 */

public interface ItemTapListener {
    void onTap(String variantID, String groupID);
}
