
package com.riyazm.swiggyassignment.fetchvariants.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Excluded {

    @SerializedName("group_id")
    private String mGroupId;
    @SerializedName("variation_id")
    private String mVariationId;

    public String getGroupId() {
        return mGroupId;
    }

    public void setGroupId(String groupId) {
        mGroupId = groupId;
    }

    public String getVariationId() {
        return mVariationId;
    }

    public void setVariationId(String variationId) {
        mVariationId = variationId;
    }

    @Override
    public String toString() {
        return "Excluded{" +
                "mGroupId='" + mGroupId + '\'' +
                ", mVariationId='" + mVariationId + '\'' +
                '}';
    }
}
