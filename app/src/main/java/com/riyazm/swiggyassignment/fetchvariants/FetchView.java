package com.riyazm.swiggyassignment.fetchvariants;

import com.riyazm.swiggyassignment.fetchvariants.models.VariantGroup;

import java.util.List;
import java.util.Map;

/**
 * Created by muhammadriyaz on 09/05/17.
 */

public interface FetchView {
    void showError(int resourceID);

    void setLoaderVisibility(boolean status);
    void printVariantGroups(List<VariantGroup> groupsList, Map<String, String> excludeMaps);
}
