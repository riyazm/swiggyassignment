
package com.riyazm.swiggyassignment.fetchvariants.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class Variants {

    @SerializedName("variant_groups")
    private List<VariantGroup> mVariantGroups;
    @SerializedName("variants")
    private Variants mVariants;

    public List<VariantGroup> getVariantGroups() {
        return mVariantGroups;
    }

    public Variants getVariants() {
        return mVariants;
    }

    public static class Builder {

        private List<VariantGroup> mVariantGroups;
        private Variants mVariants;

        public Variants.Builder withVariantGroups(List<VariantGroup> variantGroups) {
            mVariantGroups = variantGroups;
            return this;
        }

        public Variants.Builder withVariants(Variants variants) {
            mVariants = variants;
            return this;
        }

        public Variants build() {
            Variants Variants = new Variants();
            Variants.mVariantGroups = mVariantGroups;
            Variants.mVariants = mVariants;
            return Variants;
        }

    }

    @Override
    public String toString() {
        return "Variants{" +
                "mVariantGroups=" + mVariantGroups +
                ", mVariants=" + mVariants +
                '}';
    }
}
