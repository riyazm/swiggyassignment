
package com.riyazm.swiggyassignment.fetchvariants.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Variation {

    @SerializedName("default")
    private Long mDefault;
    @SerializedName("id")
    private String mId;
    @SerializedName("inStock")
    private Long mInStock;
    @SerializedName("isVeg")
    private Long mIsVeg;
    @SerializedName("name")
    private String mName;
    @SerializedName("price")
    private Long mPrice;

    public Long getDefault() {
        return mDefault;
    }

    public void setDefault(Long mDefault) {
        this.mDefault = mDefault;
    }

    public String getId() {
        return mId;
    }

    public Long getInStock() {
        return mInStock;
    }

    public Long getIsVeg() {
        return mIsVeg;
    }

    public String getName() {
        return mName;
    }

    public Long getPrice() {
        return mPrice;
    }

    public static class Builder {

        private Long mDefault;
        private String mId;
        private Long mInStock;
        private Long mIsVeg;
        private String mName;
        private Long mPrice;

        public Variation.Builder withDefault(Long mDefault) {
            this.mDefault = mDefault;
            return this;
        }

        public Variation.Builder withId(String id) {
            mId = id;
            return this;
        }

        public Variation.Builder withInStock(Long inStock) {
            mInStock = inStock;
            return this;
        }

        public Variation.Builder withIsVeg(Long isVeg) {
            mIsVeg = isVeg;
            return this;
        }

        public Variation.Builder withName(String name) {
            mName = name;
            return this;
        }

        public Variation.Builder withPrice(Long price) {
            mPrice = price;
            return this;
        }

        public Variation build() {
            Variation Variation = new Variation();
            Variation.mDefault = mDefault;
            Variation.mId = mId;
            Variation.mInStock = mInStock;
            Variation.mIsVeg = mIsVeg;
            Variation.mName = mName;
            Variation.mPrice = mPrice;
            return Variation;
        }

    }

    @Override
    public String toString() {
        return "Variation{" +
                "mDefault=" + mDefault +
                ", mId='" + mId + '\'' +
                ", mInStock=" + mInStock +
                ", mIsVeg=" + mIsVeg +
                ", mName='" + mName + '\'' +
                ", mPrice=" + mPrice +
                '}';
    }
}
