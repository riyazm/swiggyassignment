package com.riyazm.swiggyassignment.fetchvariants;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.riyazm.swiggyassignment.R;
import com.riyazm.swiggyassignment.fetchvariants.adapters.VariantsListAdapter;
import com.riyazm.swiggyassignment.fetchvariants.models.VariantGroup;
import com.riyazm.swiggyassignment.fetchvariants.models.Variation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FetchActivity extends AppCompatActivity implements FetchView, ItemTapListener {
    public static final String TAG = FetchActivity.class.getSimpleName();
    FetchPresenter presenter;
    ProgressDialog loader;
    RecyclerView variantsRecyclerView;
    VariantsListAdapter adapter;
    List<VariantGroup> variantGroups = new ArrayList<>();
    Map<String, String> excludeMaps = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch);
        presenter = new FetchPresenter(this);
        loader = new ProgressDialog(this);
        loader.setTitle(getString(R.string.loading));

        variantsRecyclerView = (RecyclerView) findViewById(R.id.variants_list);
        variantsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new VariantsListAdapter(this, variantGroups);
        variantsRecyclerView.setAdapter(adapter);
        adapter.setOnTapListener(this);
        presenter.getData();
    }


    @Override
    public void showError(int resourceID) {
        Toast.makeText(this, resourceID, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLoaderVisibility(boolean status) {
        if (status) {
            loader.show();
        } else {
            loader.dismiss();
        }
    }

    @Override
    public void printVariantGroups(List<VariantGroup> groupsList, Map<String, String> maps) {
        excludeMaps = maps;
        Log.d(TAG, "maps: " + excludeMaps.toString());
        variantGroups.clear();
        variantGroups.addAll(groupsList);
        Log.d(TAG, "groups: " + variantGroups.toString());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onTap(String variantID, String groupID) {

        Log.d(TAG, "Tapped: " + variantID + " groupID: " + groupID);
        Variation initVariation = getVariationByVariationID(variantID);
        if (initVariation != null) {
            //set all in this group to 0
            VariantGroup currentGroup = getVariationGroupByGroupID(groupID);
            if (currentGroup != null) {
                for (Variation variation : currentGroup.getVariations()) {
                    variation.setDefault(0l);
                }
            }
            //set selected to 1
            initVariation.setDefault(1l);
        }
        if (excludeMaps.containsKey(variantID)) {
            String targetVariantID = excludeMaps.get(variantID);
            Log.d(TAG, "contains key");
            Log.d(TAG, "value: " + targetVariantID);
            Variation targetVariation = getVariationByVariationID(targetVariantID);
            if (targetVariation != null) {
                Log.d(TAG, "change this variant: " + targetVariation.toString());
                targetVariation.setDefault(0l);
            }
        }
        if (excludeMaps.containsValue(variantID)) {
            Log.d(TAG, "contains value");
            String targetVariantID = getVariantKeyByValue(variantID);
            if (!TextUtils.isEmpty(targetVariantID)) {
                Variation targetVariation = getVariationByVariationID(targetVariantID);
                Log.d(TAG, "key: " + targetVariation.getId());
                Log.d(TAG, "change this variant: " + targetVariation);
                targetVariation.setDefault(0l);
            }
        }
        adapter.notifyDataSetChanged();
    }

    private VariantGroup getVariationGroupByGroupID(String groupID) {
        for (VariantGroup variantGroup : variantGroups) {
            if (variantGroup.getGroupId().equals(groupID)) {
                return variantGroup;
            }
        }
        return null;
    }

    private String getVariantKeyByValue(String value) {
        for (String key : excludeMaps.keySet()) {
            if (excludeMaps.get(key).equals(value)) return key;
        }
        return null;
    }

    private Variation getVariationByVariationID(String variantID) {
        for (VariantGroup variantGroup : variantGroups) {
            for (Variation variation : variantGroup.getVariations()) {
                if (variation.getId().equals(variantID)) {
                    return variation;
                }
            }
        }
        return null;
    }
}
